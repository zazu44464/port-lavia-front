import pushModel from "../../mixin/store/api/pushModel";

export default {
    actions: {
        pushImage(context, data) {
            return pushModel(context,'media_objects', data, 'updatePushImage')
        }
    },
    mutations: {
        updatePushImage(state, data) {
            return state.pushImageState = data
        }
    },
    state: {
        pushImageState: {
            id: null,
        }
    },
    getters: {
        getProductImageId(state) {
            return state.pushImageState.id
        }
    }
}
