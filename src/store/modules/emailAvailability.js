import axios from "../../plugins/axios";

export default {
    actions: {
        checkEmailAvailability(context, email) {
            return new Promise(((resolve, reject) => {
                axios
                    .post('/users/is_unique_email', {email})
                    .then(response => {
                        context.commit('updateEmailAvailability', response.data['isUnique'])
                        resolve()
                    })
                    .catch((reason) => {
                        reject(reason)
                    })
            }))
        }
    },
    mutations: {
        updateEmailAvailability(state, model) {
            state.emailAvailability = model
        },
    },
    state: {
        emailAvailability: false,
    },
    getters: {
        isEmailAvailable(state) {
            return state.emailAvailability
        }
    },
}
