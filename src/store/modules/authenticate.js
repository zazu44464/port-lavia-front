import localToken from "../local/localToken";
import axios from '../../plugins/axios'

export default {
    actions: {
        fetchToken(ctx, {email, password}) {
            return new Promise((resolve, reject) => {
                axios
                    .post(
                        '/users/auth',
                        {email, password}
                    )
                    .then(response => {
                        ctx.commit('updateTokens', response.data)
                        resolve()
                    })
                    .catch((reason) => {
                        console.log(reason)
                        // todo check status then return message
                        // * invalid credentials, or
                        // * connection error (Maybe it should by in main hook)
                        reject(reason)
                    })
            })
        },
        fetchRefreshToken(ctx) {
            return new Promise((resolve, reject) => {
                axios
                    .post(
                        '/users/auth/refreshToken',
                        {refreshToken: ctx.getters.getRefreshToken}
                    )
                    .then(response => {
                        ctx.commit('updateTokens', response.data)
                        resolve()
                    })
                    .catch((reason) => {
                        console.log(reason)
                        reject(reason)
                    })
            })
        },
        clearTokens(ctx) {
            ctx.commit('clearTokens')
            ctx.commit('clearRoles')
        }
    },
    mutations: {
        updateTokens(state, data) {
            state.accessToken = data.accessToken
            state.refreshToken = data.refreshToken

            localToken.setAccess(data.accessToken)
            localToken.setRefresh(data.refreshToken)
        },
        clearTokens(state) {
            state.accessToken = ''
            state.refreshToken = ''

            localToken.clear()
        }
    },
    state: {
        accessToken: localToken.getAccess(),
        refreshToken: localToken.getRefresh(),
    },
    getters: {
        isAuthenticated(state, getters) {
            return getters.getAccessToken.length > 0
        },
        getAccessToken(state) {
            return state.accessToken
        },
        getRefreshToken(state) {
            return state.refreshToken
        },
    },
}
