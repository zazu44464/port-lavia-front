import pushModel from "../../mixin/store/api/pushModel";
import fetchModel from "../../mixin/store/api/fetchModel.js";
import changeModel from "../../mixin/store/api/changeModel.js";

export default {
    actions: {
        fetchCreateUser(context, data) {
            return pushModel(context, 'users', data, 'updateUser')
        },
        fetchUser(context) {
            return fetchModel(context, 'users/about_me', null, 'updateUser')
        },
        fetchGetAllUsers(context) {
            return fetchModel(context, 'users', null, 'updateUsers')
        },
        fetchRequestResetPassword(context, data) {
            return pushModel(context, 'users/request_reset_password', data)
        },
        fetchResetPassword(context, data) {
            return pushModel(context, 'users/reset_password', data)
        },
        fetchCheckResetToken(context, data) {
            return pushModel(context, 'users/check_reset_token', data)
        },
        fetchUpdateUser(context, data) {
            return changeModel(context, 'users/' + data.id, data, 'updateUser')
        }
    },
    mutations: {
        updateUser(state, model) {
            state.user = model
        },
        updateUsers(state, model) {
            console.log(model['hydra:member'])
            state.users = model['hydra:member']
            console.log(state.users)
        },
        clearRoles(state) {
            state.user.roles = []
        }
    },
    state: {
        users: [],
        user: {
            "id": null,
            "email": null,
            "roles": [],

        },
    },
    getters: {
        getUser(state) {
            return state.user
        },
        getUsers(state) {
            return state.users
        },
        getUserRoles(state, getters) {
            return getters.getUser.roles
        },
    },
}
