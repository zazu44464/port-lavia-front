import fetchModels from "../../mixin/store/api/fetchModels";
import fetchModel from "../../mixin/store/api/fetchModel.js";
import changeOrPushModel from "../../mixin/store/api/changeOrPushModel.js";
import deleteModel from "../../mixin/store/api/deleteModel.js";

export default {
    actions: {
        fetchCourses(context) {
            return fetchModels(context, 'courses', null, 'updateCourses')
        },
        fetchApplyToCourses(context, id) {
            return fetchModel(context, 'courses/apply', id, null)
        },
        fetchChangeOrPushCourse(context, data) {
            return changeOrPushModel(context, 'courses', data, null)
        },
        fetchDeleteCourse(context, id) {
            return deleteModel(context, 'courses', id)
        }
    },
    mutations: {
        updateCourses(state, model) {
            state.courses = model.models
        }
    },
    state: {
        courses: []
    },
    getters: {
        getCourses(state) {
            return state.courses
        }
    },
}
