import fetchModels from "../../mixin/store/api/fetchModels.js";
import changeOrPushModel from "../../mixin/store/api/changeOrPushModel.js";
import deleteModel from "../../mixin/store/api/deleteModel.js";

export default {
    actions: {
        fetchAboutUs(context) {
            return fetchModels(context, 'about_uses', null, 'updateInfo')
        },
        fetchChangeOrPushAboutUs(context, data) {
            return changeOrPushModel(context, 'about_uses', data, null)
        },
        fetchDeleteAboutUs(context, id) {
            return deleteModel(context, 'about_uses', id)
        }
    },
    mutations: {
        updateInfo(state, model) {
            state.info = model.models
        }
    },
    state: {
        info: [
            {
                text: '',
                media: null
            }
        ]
    },
    getters: {
        getInfo(state) {
            return state.info
        }
    },
}
