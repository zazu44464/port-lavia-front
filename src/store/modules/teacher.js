import fetchModels from "../../mixin/store/api/fetchModels.js";
import changeOrPushModel from "../../mixin/store/api/changeOrPushModel.js";
import deleteModel from "../../mixin/store/api/deleteModel.js";

export default {
    actions: {
        fetchTeachers(context) {
            return fetchModels(context, 'teachers', null, 'updateTeachers')
        },
        fetchChangeOrPushTeacher(context, data) {
            return changeOrPushModel(context, 'teachers', data, null)
        },
        fetchDeleteTeacher(context, id) {
            return deleteModel(context, 'teachers', id)
        }
    },
    mutations: {
        updateTeachers(state, model) {
            state.teachers = model.models
        },
    },
    state: {
        teachers: []
    },
    getters: {
        getTeachers(state) {
            return state.teachers
        },
    },
}
