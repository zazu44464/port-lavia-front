import {createStore} from "vuex"
import user from "./modules/user.js";
import authenticate from "./modules/authenticate.js";
import localToken from "./local/localToken.js";
import emailAvailability from "./modules/emailAvailability.js";
import course from "./modules/course.js";
import teacher from "./modules/teacher.js";
import aboutUs from "./modules/aboutUs.js";
import imageUpload from "./local/imageUpload.js";

export default createStore({
    modules: {
        user,
        authenticate,
        localToken,
        emailAvailability,
        course,
        teacher,
        aboutUs,
        imageUpload
    }
})
