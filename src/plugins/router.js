import {createRouter, createWebHistory} from 'vue-router';
import access from "../mixin/functions/access.js";
import store from "../store";

const ifAuthenticated = (to, from, next) => {
    store.dispatch('fetchUser')
        .then(() => {
            if (store.getters.isAuthenticated) {
                if (to.meta.hasAccess()) {
                    next()
                } else {
                    router.push('')
                }

                return
            }

            router.push('/login')
        })
}

const routes = [
    {
        path: '',
        component: () => import('../pages/Home.vue')
    },
    {
        path: '/reception',
        component: () => import('../pages/Reception.vue')
    },
    {
        path: '/academy',
        component: () => import('../pages/Academy.vue')
    },
    {
        path: '/register',
        component: () => import('../pages/Register.vue')
    },
    {
        path: '/settings',
        component: () => import('../pages/Register.vue')
    },
    {
        path: '/login',
        component: () => import('../pages/Login.vue')
    },
    {
        path: '/buy-course',
        component: () => import('../pages/BuyCourse.vue')
    },
    {
        path: '/course-edit',
        component: () => import('../pages/admin/Courses.vue'),
        meta: {hasAccess: () => access.hasRoleAdmin()},
        beforeEnter: ifAuthenticated,
    },
    {
        path: '/about-us-edit',
        component: () => import('../pages/admin/AboutUses.vue'),
        meta: {hasAccess: () => access.hasRoleAdmin()},
        beforeEnter: ifAuthenticated,
    },
    {
        path: '/teacher-edit',
        component: () => import('../pages/admin/Teachers.vue'),
        meta: {hasAccess: () => access.hasRoleAdmin()},
        beforeEnter: ifAuthenticated,
    },
    {
        path: '/user-edit',
        component: () => import('../pages/admin/Users.vue'),
        meta: {hasAccess: () => access.hasRoleAdmin()},
        beforeEnter: ifAuthenticated,
    },

    // {
    //     path: '/test',
    //     component: () => import('../pages/Test.vue')
    // },
    // {
    //     path: '/bonus',
    //     component: () => import('../pages/Bonus.vue')
    // },
    // {
    //     path: '/winner',
    //     component: () => import('../pages/Winner.vue')
    // },
]

const router = createRouter({
    routes,
    history: createWebHistory()
})

export default router