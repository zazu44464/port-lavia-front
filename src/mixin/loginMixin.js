import {mapActions, mapGetters} from "vuex";

export default {
    computed: {
        ...mapGetters(['getUser'])
    },
    data() {
        return {
            response: ''
        }
    },
    methods: {
        ...mapActions(['fetchToken', 'fetchUser']),
        login() {
            this.isLoading = true;

            this.fetchToken(this.modelForm)
                .then(() => {
                    this.fetchUser()
                        .then((res) => {
                            this.modelForm.password = ''

                            if (res.roles.includes('ROLE_ADMIN')) {
                                this.$router.push('/teacher-edit')
                            } else {
                                this.$router.push('/')
                            }
                            // this.flashSuccess(this.$t('login.success', {login: this.getUser.person.givenName}));
                        })
                        .catch((reason) => {
                            this.response = reason;
                            this.$refs.modal.click()
                        })
                        .finally(() => {
                            this.isLoading = false;
                        })
                })
                .catch((reason) => {
                    if (reason === 'connectionRefused') {
                        this.response = 'Aloqa yo\'q'
                        this.$refs.modal.click()
                    } else  if (reason.response.data['hydra:description'] === 'Invalid credentials') {
                        this.response = 'Email yoki parol xato'
                        this.$refs.modal.click()
                    } else {
                        this.response = 'Loginda xatolik'
                        this.$refs.modal.click()
                    }

                    this.isLoading = false;
                })
        },
    }
}
