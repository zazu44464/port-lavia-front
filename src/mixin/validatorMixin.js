export default {
    computed: {
        hasErrors() {
            return this.hasErrorsIn(this.errors)
        },
    },
    methods: {
        // helpers
        hasErrorsIn(errors) {
            for (let name in errors) {
                if (this.errors[name] !== null) {
                    return true
                }
            }

            return false
        },
        commaToDot(val) {
            if (typeof val !== 'string') {
                return val
            }

            return val.replace(/,/, '.')
        },
        isEmpty(val) {
            return val.toString().length === 0
        },

        // model state
        modelEmailState(model) {
            let res = this.emailState(this.modelForm[model]);
            this.errors[model] = res.message
            return res.status
        },
        modelIntState(model) {
            let res = this.notEmptyIntState(this.modelForm[model]);
            this.errors[model] = res.message
            return res.status
        },
        modelNumberState(model) {
            let res = this.notEmptyNumberState(this.modelForm[model]);
            this.errors[model] = res.message

            if (res.status) {
                this.modelForm[model] = this.commaToDot(this.modelForm[model])
            }

            return res.status
        },
        modelNotEmptyState(model) {
            let res = this.notEmptyState(this.modelForm[model]);
            this.errors[model] = res.message
            return res.status
        },
        modelChooseIdState(model) {
            let res = this.chooseIdState(this.modelForm[model]);
            this.errors[model] = res.message
            return res.status
        },
        modelStrongPassword(model) {
            let res = this.strongPasswordState(this.modelForm[model]);
            this.errors[model] = res.message
            return res.status
        },
        modelPhoneOrEmpty(model) {
            let res = this.phoneOrEmptyState(this.modelForm[model]);
            this.errors[model] = res.message
            return res.status
        },
        modelIntOrEmpty(model) {
            let res = this.emptyIntState(this.modelForm[model]);
            this.errors[model] = res.message
            return res.status
        },

        // states
        phoneOrEmptyState(val) {
            if (val.match(/^\+[0-9]{7,15}$/)) {
                return {status: true, message: null}
            }

            return {status: false, message: this.$t('validator.type.phone')}
        },
        emailState(val) {
            let notEmpty = this.notEmptyState(val)

            if (!notEmpty.status) {
                return notEmpty;
            }

            if (val.toString().match(/^[-_\w.]+@[-_\w.]+\.[-_\w.]+$/)) {
                return {status: true, message: null}
            }

            return {status: false, message: this.$t('validator.type.email')}
        },
        chooseIdState(val) {
            if (val === 0) {
                return {status: false, message: this.$t('validator.choose')}
            }

            return this.notEmptyIntState(val)
        },
        notEmptyState(val) {
            if (this.isEmpty(val)) {
                return {status: false, message: this.$t('validator.required')}
            }

            return {status: true, message: null}
        },
        notEmptyNumberState(val) {
            let notEmpty = this.notEmptyState(val)

            if (!notEmpty.status) {
                return notEmpty;
            }

            if (val.toString().match(/^\d+([\\.,]?\d+)?$/)) {
                return {status: true, message: null}
            }

            return {status: false, message: this.$t('validator.type.number')}
        },
        emptyIntState(val) {
            let notEmpty = this.notEmptyState(val)

            if (val.toString().match(/^\d+$/) || !notEmpty.status) {
                return {status: true, message: null}
            }

            return {status: false, message: this.$t('validator.type.int')}
        },
        notEmptyIntState(val) {
            let notEmpty = this.notEmptyState(val)

            if (!notEmpty.status) {
                return notEmpty;
            }

            if (val.toString().match(/^\d+$/)) {
                return {status: true, message: null}
            }

            return {status: false, message: this.$t('validator.type.int')}
        },
        strongPasswordState(val) {
            if (val.match(/^(?=.*\w)(?=.*\d).{8,}$/)) {
                return {status: true, message: null}
            }

            return {status: false, message: this.$t('validator.type.strongPassword')}
        }
    },
}
