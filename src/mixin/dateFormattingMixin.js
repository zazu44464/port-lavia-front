function addZero(val) {
    return val < 10 ? '0' + val : val;
}

export default {
    methods: {
        dateTimeFormat(plainVal, isWithYear = true) {
            let date = new Date(plainVal);

            if (isNaN(date.getDay())) {
                return
            }

            let res = '' +
                addZero(date.getHours()) + ':' +
                addZero(date.getMinutes()) + ', ' +
                addZero(date.getDate()) + '.' +
                addZero(date.getMonth() + 1);

            if (isWithYear) {
                res += '.' + date.getFullYear();
            }

            return res
        }
    }
}
