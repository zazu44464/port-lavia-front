import axios from "../../../plugins/axios";

/**
 * @param context
 * @param url
 * @param data
 * @param mutationName
 * @returns {Promise<unknown>}
 */
export default function (context, url, data, mutationName = null) {

    return new Promise((resolve, reject) => {
        axios.put('/' + url, data)
            .then(response => {
                if (mutationName !== null) {
                    context.commit(mutationName, response.data)
                }

                resolve(response.data)
            })
            .catch((error) => {
                console.log(error.response)
                reject(error.response.data['hydra:description'])
            })
    })
}
