import changeModel from "./changeModel";
import pushModel from "./pushModel";

export default function (context, url, data, mutationName = null) {
    if (data.id) {
        return changeModel(context, url + '/' + data.id, data, mutationName)
    }

    return pushModel(context, url, data, mutationName)
}
