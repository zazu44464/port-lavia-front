import axios from "../../../plugins/axios";

export default function (context, url, id) {
    console.log('Delete: ' + url)
    console.log('result:')

    return new Promise((resolve, reject) => {
        axios.delete(url + '/' + id)
            .then(response => {
                console.log(response)
                resolve(response.data)
            })
            .catch((error) => {
                if (error === 'connectionRefused') {
                    reject(error)
                }

                console.log(error.response)
                reject(error.response.data['hydra:description'])
            })
    })
}
