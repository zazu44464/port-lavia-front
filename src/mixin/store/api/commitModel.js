export default function (context, response, mutationName, firstOnly = false) {
    let data;
    // todo make third variant which will be get info from single responses /api/companies/1
    if (firstOnly) {
        data = response.data['hydra:member'][0]
    } else {
        data = {
            models: response.data['hydra:member'],
            totalItems: response.data['hydra:totalItems']
        }
    }

    context.commit(mutationName, data)
}