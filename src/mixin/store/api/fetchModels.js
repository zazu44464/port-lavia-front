import axios from "../../../plugins/axios";
import urlPage from "../urlPage";
import commitModel from "./commitModel";

export default function (context, url, page, mutationName, firstOnly = false) {

    return new Promise((resolve, reject) => {
        if (url.match(/\?/)) {
            url += '&'
        } else {
            url += '?'
        }

        axios.get('/' + url + urlPage(page))
            .then(response => {
                commitModel(context, response, mutationName, firstOnly)
                resolve(response.data)
            })
            .catch((error) => {
                reject(error.response.data['hydra:description'])
            })
    })
}