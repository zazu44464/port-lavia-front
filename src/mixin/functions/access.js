import store from "../../store"

export default {
    hasRoleCeo() {
        return this.hasRole('ROLE_CEO')
    },
    hasRoleAdmin() {
        return this.hasRole('ROLE_ADMIN')
    },
    hasRoleCeoOrAdmin() {
        if (this.hasRoleCeo()) {
            return true;
        }

        return this.hasRoleAdmin();
    },
    hasRole(roleName) {
        return store.getters.getUserRoles.includes(roleName)
    },
    hasAccessToUnderCeoLevel(roleName) {
        if (this.hasRoleCeoOrAdmin()) {
            return true
        }

        return this.hasRole(roleName)
    },
}
